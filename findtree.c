
/* ======================================================================
            findtree.c, David Pisinger and Siv Sørensen  May 2022
   ====================================================================== */

/* ======================================================================
                       inclusion of library files
   ====================================================================== */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include <ctype.h>
#include <immintrin.h>

/* ======================================================================
				  Siv's parameters
   ====================================================================== */

/* Save solution run to a text file defined as input param */
#define SAVEOUTFILE

/* MST or parsimony-MST or cluster choose only one */
#undef CLUSTER
#define PARSIMONYMST
#undef MST

/* greedy or simulated annealing */
#define GREEDY
#undef SIMULATEDANNEALING

/* simulated annealing */
#define T0        0.1
#define COOL      0.999996

/* iterations and restarts */
#define MAXITER    200000        /* max mumber of iterations */
#define RESTARTS        3        /* max mumber of restarts, DOES NOT WORK (zstar) */


/* ======================================================================
				   constants
   ====================================================================== */

/* various constants */
#define FALSE       0          /* false */
#define TRUE        1          /* true */
#define NONE       -1          /* no reference */
#define INFTY  999999          /* infinity */
#define VECT        2          /* vectorized or not */

#define MAXV     1000          /* max number of vertices */
#define MAXT     6400          /* max mumber of events */
#define MAXC      100          /* max mumber of children */
#define ALPHA       2          /* length of alphabet */

#define BLOCK      32          /* bytes per 256 bit word */
#define MT (MAXT/BLOCK)        /* max number of characters vector */

#define MAXITERX    100        /* max mumber of iterations */
#define RESTARTSX   100        /* max mumber of restarts, DOES NOT WORK (zstar) */
#define STEPPRINT 10000        /* print every 100 iterations */

#define SPLITTER   1
#define MODEM      2
#define AUXILARY   3


/* ======================================================================
				  types
   ====================================================================== */

typedef int boolean;           /* boolean variable */

typedef int (*funcptr) (const void *, const void *);

typedef __m256i qchar;     /* vector of char */
typedef __m128i schar;     /* vector of char */

typedef union {            /* 256 bit, split into different sizes */
    qchar pi;
    char c[BLOCK];
    short i[16];
} vchar;

typedef union {            /* 128 bit, split into different sizes */
    __m128i pi;
    short i[8];
} wchar;

typedef union {            /* data type, either char or packed */
    qchar pck[MT];
    char ch[MAXT];
} datatype;

typedef struct {
  datatype data;            /* data (only on leaf nodes) */
  int id;                   /* own index */
  int leaf;                 /* leaf node is modem, otherwise splitter */
  int nodetype;             /* SPLITTER, MODEM */
  int done;                 /* node is processed */
  float x;                  /* geographical x-coordinate */
  float y;                  /* geographical y-coordinate */
  int child[MAXC];          /* children terminated by NONE */
  int save[MAXC];           /* copy of children terminated by NONE */
  int best[MAXC];           /* copy of best children terminated by NONE */
  int sum;                  /* sum of parsimony, current node */
  int treesum;              /* sum of parsimony, node and subtree */
} nodestruct;

typedef struct {
  datatype data;            /* string of events */
  int ref;                  /* number of original node */
  int nodetype;             /* SPLITTER, MODEM, AUXILARY */
  int id;                   /* own index */
  int left;                 /* left node or NONE */
  int right;                /* right node or NONE */
  int sum;                  /* sum of parsimony, current node */
  int treesum;              /* sum of parsimony, node and subtree */
} bnodestruct;

typedef struct {            /* disjoint sets */
  int parent;
  int size;
} setstruct;

typedef struct {            /* disjoint sets */
  int id;                   /* cluster id */
  int child[MAXV];          /* modems belonging to cluster */
  int size;                 /* num modems in cluster */
  float x;                  /* geographical x-coordinate (mean)*/
  float y;                  /* geographical y-coordinate (mean)*/
  datatype data;            /* data (mean/most common) */
} clusterstruct;

typedef struct {
  int a;                    /* start node */
  int b;                    /* end node */
  float d;                  /* distance */
  int chosen;               /* selected or not */
} edgestruct;


/* ======================================================================
				  global variables
   ====================================================================== */

int N;                      /* number of splitters */
int M;                      /* number of modems */
int V;                      /* number of vertices */
int S;                      /* number of events */
int ST;                     /* number of events / BLOCK */
int B;                      /* number of binary nodes */
int R;                      /* root of binary tree */

nodestruct node[MAXV];
nodestruct nodeC[10]; 
edgestruct edge[MAXV*MAXV]; 
setstruct set[MAXV];
clusterstruct cluster[MAXV];
bnodestruct bnode[2*MAXV]; 

datatype wildcard;          /* all states */

long parsimonies;            /* counters : The long data type stores integers like int , but gives a wider range of values at the cost of taking more memory. */
long nparsimonies;           /* counters */
long nparsimonies256;        /* counters */

char str1[1000];
char str2[1000];

FILE *fptr;


/* =======================================================================
                             macro functions
   ======================================================================= */

#define srandm(x)       srand48x(x)
#define randm(x)        (lrand48x() % (long) (x))
#define drandm(x)       (drand48x())


/* =======================================================================
                                random
   ======================================================================= */

/* Own implementation of srand48, and lrand48 */

unsigned int _h48, _l48;   /* should be 32 bit integer */

void srand48x(int s)
{
  _h48 = s;
  _l48 = 0x330E;
}

long lrand48x(void)
{
  _h48 = (_h48 * 0xDEECE66D) + (_l48 * 0x5DEEC);
  _l48 = _l48 * 0xE66D + 0xB;
  _h48 = _h48 + (_l48 >> 16);
  _l48 = _l48 & 0xFFFF;
  return (_h48 >> 1);
}

double drand48x(void)
{
  return lrand48x() / (double) ((unsigned long) 0x1 << 31);
}


/* =======================================================================
                             timing routine
   ======================================================================= */

/* The following routine is used for measuring the cpu time used
 *   cputime(NULL)    - start timer
 *   cputime(&t)      - return the elapsed cpu-time in variable t (double).
 */

void cputime(double *t)
{
  static clock_t start, stop;
  if (t == NULL) {
    start = clock();
  } else {
    stop = clock();
    *t = (double) (stop - start) / CLOCKS_PER_SEC;
  }
}

/* ======================================================================
				dist
   ====================================================================== */

float dist(int a, int b)
{
  float dx, dy;
  dx = node[a].x - node[b].x;
  dy = node[a].y - node[b].y;
  return sqrt(dx*dx + dy*dy);
}


/* ======================================================================
				distcmp
   ====================================================================== */

int distcmp(edgestruct *a, edgestruct *b)
{
  return 100000 * (a->d - b->d);
}


/* ======================================================================
                               showdata
   ====================================================================== */

void showdata(datatype d)
{
  int i; 
  for (i = 0; i < S; i++) {
    printf("%d ", d.ch[i]);
  }
  printf("\n");
}

/* ======================================================================
				showlist
   ====================================================================== */

void showlist(int *p)
{
  /* find length of list */
  for (; *p != NONE; p++) { printf("%d ", *p); }
  printf("\n");
}


/* ======================================================================
				length
   ====================================================================== */

int length(int *p)
{
  int *r;
  /* printf("length\n"); */
  /* find length of list */
  for (r = p; *p != NONE; ) { p++; }
  /* printf("length %d\n", (int) (p-r)); */
  return p - r;   /* index of NONE */
}


/* ======================================================================
				permute
   ====================================================================== */

void permute(int *p, int j, int k)
{
  int t;
  printf("permute pos %d %d\n", j, k);
  /* permute positions j and k in list p */
  t = p[j]; p[j] = p[k]; p[k] = t; 
  printf("permute done\n");
}


/* ======================================================================
				insert
   ====================================================================== */

void insert(int *p, int j)
{
  /* insert j at end of list */
  for ( ; *p != NONE; ) { p++; }
  *p = j; p++; *p = NONE;
}


/* ======================================================================
				insert_pos
   ====================================================================== */

void insert_pos(int *p, int j, int k)
{
  int *r, *q;
  printf("insert at pos %d\n", k);
  showlist(p);
  /* insert j at position p[k] and move rest of list */
  r = p+k; q = p+length(p); 
  *(q+1) = NONE;
  for ( ; q != r; q--) { *q = *(q-1); }
  *r = j;
  showlist(p);
  printf("done\n");
}


/* ======================================================================
				sort_copy
   ====================================================================== */

void sort_copy(int *d, int *s)
{
  int *i, *k;

  k = d;
  for (i = s; *i != NONE; i++) {
    if (node[*i].leaf == TRUE) { *k = *i; k++; }
  }
  for (i = s; *i != NONE; i++) {
    if (node[*i].leaf == FALSE) { *k = *i; k++; }
  }
  *k = NONE;
}


/* ======================================================================
				delete 
   ====================================================================== */

void delete(int *p)
{
  /* delete position p, and move rest of list forward */
  for (; ; p++) { 
    *p = *(p+1); 
    if (*p == NONE) break;
  }
}


/* ======================================================================
				make_set
   ====================================================================== */

void make_set(int x)
{
  set[x].parent = x;
  set[x].size = 1;
}


/* ======================================================================
				find_set
   ====================================================================== */

int find_set(int x)
{
  int y;
  while (set[x].parent != x) {
    y = set[x].parent;
    set[x].parent = set[y].parent;
    x = y;
  }
  return x;
}


/* ======================================================================
				union
   ====================================================================== */

void set_union(int x, int y)
{
  int t;
  if (x == y) return;
  if (set[x].size < set[y].size) {
    t = x; x = y; y = t;
  }
  set[y].parent = x;
  set[x].size += set[y].size;
}


/* ======================================================================
				mst
   ====================================================================== */

void mst(void)
{
  int i, j, k, n, u, v, su, sv;
  float d, md;

  /* init empty children */
  printf("MST splitters %d modems %d\n", N, M); 
  for (i = 0; i < V; i++) {
    node[i].child[0] = NONE;
    node[i].done = FALSE;
  }
 
  /* solve MST for splitters */
  /* generate all possible edges in matrix */
  k = 0; 
  for (i = 0; i < N; i++) {
    for (j = i+1; j < N; j++) {
      edge[k].a = i;
      edge[k].b = j;
      edge[k].d = dist(i, j);
      edge[k].chosen = FALSE;
      k++;
    }
  }

  for (i = 0; i < N; i++) {
    make_set(i);              // not sure what this is
  }
 
  printf("kruskal nodes %d edges %d\n", N, k); 
  qsort(edge, k, sizeof(edgestruct), (funcptr) distcmp);
#ifdef debug
  for (i = 0; i < k; i++) {
    printf("(%d,%d) %.2f\n", edge[i].a, edge[i].b, edge[i].d); 
  }
#endif

  n = 0;
  printf("kruskal start\n");
  for (i = 0; i < k; i++) {
    u = edge[i].a; 
    v = edge[i].b; 
    d = edge[i].d; 
    su = find_set(u);
    sv = find_set(v);
    /* printf("(%d,%d) d %.2f set (%d,%d)\n", u, v, d, su, sv);  */
    if (su != sv) {
      edge[i].chosen = TRUE;
      /* printf("insert (%d %d)\n", u, v);  */
      insert(node[u].child, v);
      insert(node[v].child, u);
      n++; if (n == N-1) break;
      set_union(su, sv);
    }
  }

#ifdef output
  printf("kruskal MST solution\n");
  for (i = 0; i < k; i++) {
    if (edge[i].chosen) { 
      printf("(%d,%d) %.2f\n", edge[i].a, edge[i].b, edge[i].d);
    }
  }
#endif

  /* now, assign modems to splitters */
  printf("assign %d modems to %d splitters\n", M, N);
  for (i = N; i < N+M; i++) {
    md = INFTY;
    for (j = 1; j < N; j++) {           // Dont attach to root
      d = dist(i, j);
      if (d < md) { md = d; u = j; }
    }
#ifdef output
    printf("insert (%d %d)\n", u, i); 
#endif
    insert(node[u].child, i);
  }
}



/* ======================================================================
				mst2general
   ====================================================================== */

void mst2general(int i)
{
  int *p, *q;

  node[i].done = TRUE;
#ifdef debug
  for (p = node[i].child; *p != NONE; p++) {
    printf("node %d child %d\n", i, *p);
  }
#endif
  for (p = node[i].child; *p != NONE; ) {
    if (node[*p].done) {
      delete(p);
    } else {
      mst2general(*p);
      p++;
    }
  }
}


/* ======================================================================
			      walk_show
   ====================================================================== */

void walk_show(int i)
{
  int *p;

  for (p = node[i].child; *p != NONE; p++) {
    /* printf("walk node %d child %d\n", i, *p); */
    printf("(%d,%d)\n", i, *p); 
  }
  for (p = node[i].child; *p != NONE; p++) {
    walk_show(*p);
  }

#ifdef lort
  for (p = node[i].child; *p != NONE; p++) {
    printf("post walk node %d child %d\n", i, *p); 
    printf("data node %d leaf %d\n", i, node[i].leaf); 
    showdata(node[i].data);
    printf("data node %d leaf %d\n", *p, node[*p].leaf); 
    showdata(node[*p].data);
  }
#endif
}

/* ======================================================================
			      walk_save_file
   ====================================================================== */

void walk_save_file(int i)
{
  int *p;

  for (p = node[i].child; *p != NONE; p++) {
    /* printf("walk node %d child %d\n", i, *p); */
    fprintf(fptr, "%d,%d,", i, *p); 

  }
  for (p = node[i].child; *p != NONE; p++) {
    walk_save_file(*p);
  }
}

/* ======================================================================
			      walk_b_show
   ====================================================================== */

void walk_b_show(int i)
{
  if (i == NONE) return;
  printf("walk_b_show %d (%d %d)\n", i, bnode[i].left, bnode[i].right);
  walk_b_show(bnode[i].left);
  walk_b_show(bnode[i].right);
}


/* ======================================================================
			      linear_b_show
   ====================================================================== */

void linear_b_show(int k)
{
  int i;

  for (i = 0; i < k; i++) {
    printf("linear_b_show %d %d %d (%d %d)\n", 
            i, bnode[i].id, bnode[i].ref, bnode[i].left, bnode[i].right);
  }
}


/* ======================================================================
				greedy
   ====================================================================== */

void greedy(void)
{
  printf("\nGREEDY\n");
  printf("mst\n");
  mst();
  printf("mst2general\n");
  mst2general(0);
#ifdef output
  printf("walk_show\n");
  walk_show(0);
#endif
  printf("end greedy\n");
}

/* ======================================================================
				attach modems 'cheapest'
   ====================================================================== */

void attach_modems(float alpha)
{
  int i, j, k, *p;
  double zbest, z2base, z2, z;

  /* init empty children */
  printf("construction: splitters %d modems %d\n", N, M); 
  for (i = 0; i < V; i++) {
    node[i].child[0] = NONE;
    node[i].done = FALSE;         // consider setting to true as not used
  }

  /* add the shortest edge to a splitter for each modem*/
  k = 0;
  for (i = N; i < V; i++) {
    edge[k].d = 10.0;             // some larger number
    edge[k].b = i;                // edge to node
    for (j = 1; j < N; j++) {     // j=1, never connect to root
      if (dist(i, j) < edge[k].d) {
        edge[k].a = j;            // edge from node
        edge[k].d = dist(i, j);
        edge[k].chosen = FALSE;
      }
    }
    k++;
  }

  /*for (i = 0; i < N; i++) {
    make_set(i);                  // not sure what this is
  }*/
 
  // sort edges -> ascending
  qsort(edge, M, sizeof(edgestruct), (funcptr) distcmp);

  /*
  for (i = 0; i < M; i++) {
    printf("(%d,%d) %.4f\n", edge[i].a, edge[i].b, edge[i].d); 
  }
  */

  // attach each modem to cheapest option (dist+parsi)
  for (i = N; i < V; i++) {
    //initialize best
    zbest = S;                          // num events - think 'large' enough
    k = -1;
    for (j = 1; j < N; j++) {           // don't attach to root
      z2base = walk_nparsimony(j);      // base is without new modem
      insert(node[j].child, i);
      z2 = walk_nparsimony(j);          // add modem and calc new parsi

      // cost - normalize parsi with num of events S
      z = alpha * dist(j, i) + (1 - alpha) * (z2 - z2base) / S;     //cost
      if (z < zbest) {
        k = j;                          // cheapest parent of i
        zbest = z;
        //printf("New best, dist percent of z: %.5f\n", alpha * dist(j, i) / z * 100);
      }
      //regret insertion of i as child of j:
      for (p = node[j].child; *p != NONE; p++) {  
        if (*p == i) {
          delete(p);                   /*delete child i from parent j */
        }
      }
    }
    //insert i as child of 'best' found parent k
    insert(node[k].child, i);
  }
}

/* ======================================================================
				cost parsi+dist btw splitter parent i and child j
   ====================================================================== */
float cost_ij(int i, int j, float alpha)
{
  int *g;
  float z, p, pi, pj;

  pi = walk_nparsimony(i); 
  pj = walk_nparsimony(j);
  insert(node[i].child, j);
  p = walk_nparsimony(i) - pi - pj;     // cost of 'change' in parsi

  for (g = node[i].child; *g != NONE; g++) {  
    if (*g == j) {
      delete(g);                        // regret insert
    }
  }

  z = alpha * dist(i, j) + (1 - alpha) * p / S;     //tot cost
  // printf("cost phase2, edge dist percent: %.8f\n", alpha * dist(i, j) / z * 100);
  return z;
}

/* ======================================================================
				mst parsi+dist construct
   ====================================================================== */
void mst_parsi(float beta) 
{
  int i, j, k, k1, u, v, w, y;
 
  /* solve MST for splitters */
  /* generate all possible edges in matrix */ 
  k = 0; 
  for (i = 0; i < N; i++) {
    for (j = 1; j < N; j++) {       // j=1 because root can never be child
      if (i != j) {
        edge[k].a = i;
        edge[k].b = j;
        edge[k].d = cost_ij(i, j, beta);
        edge[k].chosen = FALSE;
        k++;
      }
    }
  }

  /*
  // test if direction of identical edges matters (it does!)
  for (i = 0; i < k; i++) {
    for (j = i+1; j < k; j++) {
      if (edge[i].a == edge[j].b && edge[i].b == edge[j].a ) {
        printf("Edge cost identical: %s\n",  edge[i].d == edge[j].d ? "true" : "false");
      }
    }
  }
  */

  printf("greedy nodes %d edges %d\n", N, k); 
  qsort(edge, k, sizeof(edgestruct), (funcptr) distcmp);

  /*
  for (i = 0; i < k; i++) {
    printf("(%d,%d) %.4f\n", edge[i].a, edge[i].b, edge[i].d); 
  }
  */

  for (i = 0; i < (N - 1); i++) {
    // printf("Inserting edge %d out of %d, ", i+1, N);
    k1 = 0;
    u = edge[0].a; 
    v = edge[0].b;
    insert(node[u].child, v);
    node[v].done = TRUE;
    // printf("inserted edge from %d to %d\n", u, v); 

    // update costs after edge insert:
    for (j = 0; j < k; j++) {
      w = edge[j].a;
      y = edge[j].b;
      if (y == v || walk_reachable(y, w)) {   /* may not introduce cycle */
        edge[j].d = S;                        // big number, never more parents for v
        k1++;
      } else {
        edge[j].d = cost_ij(w, y, beta);     //update cost
      }
    }
    // printf("Done cost update, now qsort \n"); 
    qsort(edge, k, sizeof(edgestruct), (funcptr) distcmp);
    k = k - k1;
  }
}

/* ======================================================================
				greedy parsimony+dist construct
   ====================================================================== */

void greedy_parsi(float beta)
{
  // float beta = 0.0;
  printf("\nGREEDY PARSI construction with alpha %f\n", beta);
  printf("Construction phase 1\n");
  attach_modems(beta);
  // beta = 0.3;
  printf("Construction phase 2\n");
  mst_parsi(beta);
#ifdef output
  printf("walk_show\n");
  walk_show(0);
#endif
  printf("end greedy parsi\n");
}

/* ======================================================================
				initialize cluster
   ====================================================================== */

void ini_cluster(int x)
{
  int v, i;

  cluster[x].id = x;
  for (i = 0; i < M+1; i++) {
    cluster[x].child[i] = NONE;
  }
  cluster[x].size = 0;
  
  //choose rand modem as inital pos and data:
  v = randm(M) + N;     
  cluster[x].x = node[v].x;
  cluster[x].y = node[v].y;
  cluster[x].data = node[v].data;
  //printf("Cluster %d is initialized in node %d with x:%.4f, y:%.4f\n", x, v, cluster[x].x, cluster[x].y);
}

/* ======================================================================
				cluster dist from modem v to cluster c
   ====================================================================== */

float c_dist(int v, int c)
{
  float dx, dy;
  dx = cluster[c].x - node[v].x;
  dy = cluster[c].y - node[v].y;
  return sqrt(dx*dx + dy*dy);
}

/* ======================================================================
				total number of different data points btw modem v and cluster c
   ====================================================================== */
int parsimony_cluster(int v, int c)
{
  int i;
  int z = 0;
  for (i = 0; i < S; i++) {
    /*
    printf("node[v].data.ch[i]: %d\n", node[v].data.ch[i]);
    printf("cluster[c].data.ch[i]): %d\n", cluster[c].data.ch[i]);
    printf("bit compare (1 if different): %d\n", (node[v].data.ch[i] != cluster[c].data.ch[i]));
    */
    if (node[v].data.ch[i] != cluster[c].data.ch[i]) {
      z++;
    }
  }
  return z;
}

/* ======================================================================
				total number of different data points btw modem v and cluster c
   ====================================================================== */
int parsimony_cluster_faster(int v, int c)
{
  int i, z;
  nodeC[1] = node[v];
  //nodeC[1] = cluster[c];
  z = walk_nparsimony_c(0);

  return z;
}

/* ======================================================================
				return cheapest cluster id for modem x
   ====================================================================== */

int best_cluster(int v, float beta)
{
  int w, i, z2;
  float c, c1, z1;

  c = S;
  // run through each cluster and save and return best cost cluster
  for (i = 0; i < N-1; i++) {
    z1 = c_dist(v, i);
    z2 = parsimony_cluster(v, i);
    c1 = beta * z1 + (1 - beta) * z2 / S; 
    
    //printf("cost for modem %d in cluster %d, is: %.8f\n", v, i, c1);
    //printf("-> where obj split percent is: %.4f (dist), %.4f (parsi)\n", beta * z1 / c1 *100, (1 - beta) * z2 / S / c1*100);
    if (c1 < c) {
      c = c1;
      w = i;
    }
  }
  //printf("best parsi cost for modem %d is: %d / %d in cluster %d\n", v, z2, S, w); // DOES NOT WORK 
  return w;
}

/* ======================================================================
				delete modem x from cluster v
   ====================================================================== */

void del_from_cluster(int x, int c)
{
  int *p;

  for (p = cluster[c].child; *p != NONE; p++) {
    if (*p == x) {
      delete(p);
      cluster[c].size--;
    } 
  }
}

/* ======================================================================
				calc cluster 'mean' x,y pos based on children
   ====================================================================== */

void cluster_pos(int c)
{
  int *p;

  cluster[c].x = 0;
  cluster[c].y = 0;
  for (p = cluster[c].child; *p != NONE; p++) {
    cluster[c].x += node[*p].x;
    cluster[c].y += node[*p].y;
  }
  cluster[c].x = cluster[c].x / cluster[c].size;
  cluster[c].y = cluster[c].y / cluster[c].size;
}

/* ======================================================================
				calc cluster 'mean' data' based on children
   ====================================================================== */

void cluster_data(int x)
{
  int s, i, z, id;

  for (s = 0; s < S; s++) {
    z = 0;
    for (i = 0; i < cluster[x].size; i++) {
      id = cluster[x].child[i];
      if (node[id].data.ch[s] == 0x1 ) {
        z++;
      }
    }
    //printf("zeros freq: %d, ones freq: %d\n", z, cluster[x].size - z);
    if (z > (cluster[x].size - z)) {
      cluster[x].data.ch[s] = 0x1;
      //printf("data for cluster %d time %d is %d\n", x, s, 0x1);
    } else {
      cluster[x].data.ch[s] = 0x2;              // in case of tie, data = 1
      //printf("data for cluster %d time %d is %d\n", x, s, 0x2);
    }
  } 
}

/* ======================================================================
				finding the splitter that's closest to all modem in cluster c
   ====================================================================== */
int nearest_splitter(int c)
{
  int i, j, s;
  float d, d1;

  d = M;
  for (i = 1; i < N; i++) {                   // skip i=0 (don't assign to root)
    d1 = 0;
    for (j = 0; j < cluster[c].size; j++) {
      d1 += dist(i, cluster[c].child[j]);
    }
    if (d1 < d) {
      d = d1;
      s = i;                  // current closests splitter
    }
  }
  return s;
}

/* ======================================================================
				add modem v to cluster c
   ====================================================================== */
void add_to_cluster(int v, int c) 
{
  int j;

  node[v].done = c;
  cluster[c].child[ cluster[c].size ] = v;
  cluster[c].size++;
}

/* ======================================================================
				attach modems 'cheapest' (k-means clustering)
   ====================================================================== */

void cluster_modems(float beta)
{
  int i, j, k, *p, c, v, it;
  int done = FALSE;
  double zbest, z2base, z2, z;

  /* init empty children */
  //printf("construction: splitters %d modems %d\n", N, M); 
  for (i = 0; i < V; i++) {
    node[i].child[0] = NONE;
    node[i].done = V;         // lil hack. We use 'done' as 'cluster id'
  }

  /* initialize N-1 empty clusters */ 
  for (i = 0; i < N-1; i++) {
    ini_cluster(i);
  }

  // Assign modems to cluster, continue untill no change
  it = 0;
  while (done == FALSE) {
    done = TRUE;
    for (i = N; i < V; i++) {
      c = best_cluster(i, beta);
      //printf("Best cluster for modem %d is cluster %d\n", i, c);
      if (c == node[i].done) {                // same cluster assignment
        // do nothing
      } else {                                // new cluster assignment
        // printf("NEW assign, modem %d, to cluster %d from %d\n ", i, c, node[i].done);
        done = FALSE;                         // must repeat loop 
        if (node[i].done < N-1) {
          del_from_cluster(i, node[i].done);
        }
          add_to_cluster(i, c);
      }
    }

    // recalculate all cluster means:
    for (i = 0; i < N-1; i++) {
      if (cluster[i].child[0] == NONE) {      // empty cluster
        printf("Found empty cluster %d- reset\n", i);
        ini_cluster(i);                       // re-initialize
      } else {
        cluster_pos(i);
        cluster_data(i);
        // print cluster num and children:
        /*
        printf("Cluster %d with children: (new x %.4f, y %.4f)\n", i, cluster[i].x, cluster[i].y);
        for (j = 0; j < cluster[i].size; j++) {
          printf("%d, ", cluster[i].child[j]);
        } 
        printf("\n");
        */
      }
    }
    //printf("Reset ALL cluster means done, it: %d\n\n", it++);
    //done = TRUE;
  }

  //printf("No change in clusters, exit while\n");

  /* attach each cluster to nearest splitter 
     assume order this is done in should not matter*/
  for (i = 0; i < N-1; i++) {
    v = nearest_splitter(i);
    // printf("cluster %d assigned to closest splitter %d\n", i, v);
    for (j = 0; j < cluster[i].size; j++) {
      insert(node[v].child, cluster[i].child[j]);
    }
  }
  printf("Phase 1 done, assigned clusters to splitters\n");
}


/* ======================================================================
			      walk_cost
   ====================================================================== */

float walk_cost(int i, int show)
{
  int *p;
  float cost;

  cost = 0.0;
  for (p = node[i].child; *p != NONE; p++) {
    if (show) printf("edge (%d,%d) cost %lf\n", i, *p, dist(i,*p));
    cost += dist(i, *p) + walk_cost(*p, show);
  }
  return cost;
}

/* ======================================================================
			      node Struct to use in clustering
   ====================================================================== */
void makeNewNodeStructMini()
{ 
  int i, j;
  char *p;

  for (i = 0; i < 3; i++) nodeC[i].id = i;
  for (i = 0; i < 1; i++) nodeC[i].leaf = FALSE;
  for (i = 1; i < 3; i++) nodeC[i].leaf = TRUE;
  for (i = 0; i < 1; i++) nodeC[i].nodetype = SPLITTER;
  for (i = 1; i < 3; i++) nodeC[i].nodetype = MODEM;


  //initialize with random data
  for (i = 0; i < 200; i++) nodeC[1].data.pck[i] = node[M].data.pck[i];
  for (i = 0; i < 200; i++) nodeC[2].data.pck[i] = node[M].data.pck[i];

  /* init wildcard to all states */
  p = wildcard.ch; 
  for (j = 0; j < S; j++, p++) {
    *p = (unsigned int) (0x7);
  } 

   /* init empty children */
  for (i = 0; i < 3; i++) {
    node[i].child[0] = NONE;
    node[i].done = FALSE;
  }

  //make children
  insert(nodeC[0].child, 1);
  insert(nodeC[0].child, 2);
}

/* ======================================================================
				cluster based on parsi+dist
   ====================================================================== */

void greedy_cluster(float beta)
{
  int i, j;
  double z, zmin, z1, z2, z1base, z2base;
  //float beta = 1.0;
  zmin = 10000;

  //makeNewNodeStructMini();
  
  printf("\nCLUSTER construction with beta %f\n", beta);
  for (i = 0; i < M; i++) {
    printf("Construction phase 1\n");
    cluster_modems(beta);
    printf("Construction phase 2\n");
    mst_parsi(beta);
    
    z1 = walk_cost(0,FALSE);
    z2 = walk_nparsimony(0);
    if (i == 0) { z1base = z1; z2base = z2;}
    z = beta * z1 / z1base + (1 - beta) * z2 / z2base;
    printf("Constr it %d sol percent: %.2f (z1) %.2f (z2)\n", i, (beta * z1 / z1base / z *100), ((1 - beta) * z2 / z2base / z * 100));
    if (i == 0 || z < zmin) {
      savesolution();
      zmin = z;
      j = i;
    }
    // walk_show(0);
  }
  printf("Best clustering found in random initializattion no %d\n", j);
  restoresolution();          // best of all random cluster constructions
#ifdef output
  printf("walk_show\n");
  walk_show(0);
#endif
  printf("end greedy parsi\n");
}
 

/* ======================================================================
			      walk_reachable
   ====================================================================== */

int walk_reachable(int i, int k)
{
  int *p;

  if (i == k) return TRUE;
  for (p = node[i].child; *p != NONE; p++) {
    if (walk_reachable(*p, k)) return TRUE;
  }
  return FALSE;
}


/* ======================================================================
			      walk_delete
   ====================================================================== */

void walk_delete(int i, int k)
{
  int *p;

  for (p = node[i].child; *p != NONE; ) {
    if (*p == k) {
      // sprintf(str2, "deleted (%d,%d)", i, k);
      delete(p); 
    } else {
      walk_delete(*p, k);
      p++;
    }
  }
}

/* ======================================================================
			      walk_find_parent 
   ====================================================================== */

void walk_find_parent(int i, int k, int *j)
{
  int *p;

    for (p = node[i].child; *p != NONE; p++ ) {
    printf("node is %d\n", i);
    if (*p == k) {
      /*sprintf(str2, "deleted (%d,%d)", i, k);*/
      *j = i; 
      return;
    } else {
      walk_find_parent(*p, k, j);
    }
  }
}

/* ======================================================================
			      walk_find_parent 
   ====================================================================== */

int walk_find_parent2(int i, int k)
{
  int *p, v;

  if (node[i].leaf == TRUE) return NONE;
  for (p = node[i].child; *p != NONE; p++) {
    if (*p == k) {
      return i;
    } else {
      v = walk_find_parent2(*p, k);
      if (v != NONE) return v;
    }
  }
  return NONE;
}


/* ======================================================================
			      walk_convert
   ====================================================================== */

int walk_convert(int i)
{
  int ch[MAXC], *p, k, l, r;

  sort_copy(ch, node[i].child);  /* first modems then splitters */
  p = ch;
  if (*p == NONE) {
    /* printf("leaf node\n"); */
    /* leaf node */
    k = B; B++; 
    bnode[k].ref = i;
    bnode[k].nodetype = MODEM;
    bnode[k].id = k;
    bnode[k].left = NONE;
    bnode[k].right = NONE;
    return k;
  } 

  if (*(p+1) == NONE) {
    /* printf("singular node\n"); */
    /* singular node */
    l = walk_convert(*p);
    k = B; B++; 
    bnode[k].ref = i;
    bnode[k].nodetype = SPLITTER;
    bnode[k].id = k;
    bnode[k].left = l;
    bnode[k].right = NONE;
    return k;
  }

  /* binary or more */
  /* printf("multiple node\n"); */
  l = walk_convert(*p);
  p++;
  r = walk_convert(*p);
  k = B; B++; 
  bnode[k].ref = i;
  bnode[k].nodetype = AUXILARY;
  bnode[k].id = k;
  bnode[k].left = l;
  bnode[k].right = r;
  for (p++ ; *p != NONE; p++) {
    l = k;
    r = walk_convert(*p);
    k = B; B++; 
    bnode[k].ref = i;
    bnode[k].nodetype = AUXILARY;
    bnode[k].id = k;
    bnode[k].left = l;
    bnode[k].right = r;
  }
  bnode[k].nodetype = SPLITTER;
  return k;
}


/* ======================================================================
				general2binary
   ====================================================================== */

int general2binary(void)
{
  int i, j;

#ifdef output
  printf("\ngeneral2binary\n");
#endif
  B = 0;
  R = walk_convert(0);
  for (i = 0; i < R; i++) {
    j = bnode[i].ref;   /* note: I am copying to all nodes xxx */
    memcpy(bnode[i].data.ch, node[j].data.ch, S);
    memset(bnode[j].data.ch + S, 0, MAXT-S);
  }
#ifdef output
  printf("general2binary size %d\n", R);
#endif
  return R;
}


/* ======================================================================
				savesolution
   ====================================================================== */

void savesolution(void)
{
  int i;
  int *p, *q;

  for (i = 0; i < N; i++) {
    p = node[i].child;
    q = node[i].best;
    for ( ; *p != NONE; p++, q++) {
      *q = *p;
    }
    *q = NONE;
  }
}


/* ======================================================================
				restoresolution
   ====================================================================== */

void restoresolution(void)
{
  int i;
  int *p, *q;

  for (i = 0; i < N; i++) {
    p = node[i].child;
    q = node[i].best;
    for ( ; *q != NONE; p++, q++) {
      *p = *q;
    }
    *p = NONE;
  }
}


/* ======================================================================
				savetree
   ====================================================================== */

void savetree(void)
{
  int i;
  int *p, *q;

  /* printf("savetree\n"); */
  for (i = 0; i < N; i++) {
    p = node[i].child;
    q = node[i].save;
    for ( ; *p != NONE; p++, q++) {
      *q = *p;
    }
    *q = NONE;
  }
  /* printf("savetree done\n"); */
}


/* ======================================================================
				restoretree
   ====================================================================== */

void restoretree(void)
{
  int i;
  int *p, *q;

  /* printf("restoretree\n"); */
  for (i = 0; i < N; i++) {
    p = node[i].child;
    q = node[i].save;
    for ( ; *q != NONE; p++, q++) {
      *p = *q;
    }
    *p = NONE;
  }
  /* printf("restoretree done\n"); */
}


/* ======================================================================
				insert_edge
   ====================================================================== */

void insert_edge(int u, int v)
{
  int k, l;
  if (u >= N) { printf("not internal node\n"); exit(0); }
  if (v >= V) { printf("too large node\n"); exit(0); }
  if (u == 0 && v >= N) { printf("cannot attach modem to root\n"); exit(0); }

  sprintf(str1, "insert edge (%d,%d) ", u, v);
  sprintf(str2, "delete none");
  if (u == v) return; /* edge must be real */
  if (node[u].leaf) return; /* root cannot be child */ 
  if (v == 0) return; /* root cannot be child */
  if (walk_reachable(v, u)) return;  /* may not introduce cycle */

#ifdef output
  printf("insert edge (%d,%d)\n", u, v);
#endif
  walk_delete(0, v);
#ifdef test
  printf("insert random pos\n");
  l = length(node[u].child);
  k = randm(l); 
  insert_pos(node[u].child, v, k); 
#endif
  insert(node[u].child, v);
}

/* ======================================================================
				                 swap parent child status
   ====================================================================== */

void swap_parent_child(int u)
{
  int v, w, *p, *g;


  // printf("child is node: %d\n", u); 
  if (u >= N) { printf("not internal node\n"); exit(0); }
  if (u == 0) return; /* node cannot be root */
  if (node[u].leaf) return; /* node cannot be child - needed?? */

  // sprintf(str1, "insert edge (%d,%d) ", u, v);
  // sprintf(str2, "delete none");
  
  v = walk_find_parent2(0, u); /*find parent*/
  // printf("parent is node: %d\n", v); 
  if (v >= N) { printf("Parent is not internal node\n"); exit(0); }
  if (v == 0) return; /* root cannot be the parent that will become a child. */
  if (u == v) return; /* New edge must be real - needed??*/
  
  w = walk_find_parent2(0, v); /*find grandparent*/
  // printf("grandparent is node: %d\n", w); 
  if (w >= N) { printf("Grandparent is not internal node\n"); exit(0); }
  if (w == u) return; /* New edge must be real - needed??*/

  #ifdef output
    printf("Swaping parent-child (%d,%d)\n", v, u);
  #endif

  for (p = node[v].child; *p != NONE; p++) {  
    if (*p == u) {
      delete(p); /*delete child from parent*/
    }
  }

  for (g = node[w].child; *g != NONE; g++) {
    if (*g == v) {
      delete(g); /*delete parent from grandparent*/
    }
  }

  insert(node[u].child, v); /* parent becomes child*/
  insert(node[w].child, u); /* child becomes child of grandparent*/
}

/* ======================================================================
                new subtree of size: 3 nodes, rooted in u
   ====================================================================== */
boolean shuffle_subtree(int u, int k)
{
  int l1[N], l2[N];
  int i, j, *p, v, w, loop;
  int c1 = 1;
  int c2 = 0;

  //initialize:
  l1[0] = u;

  // Add root's children to l2:
  for (p = node[u].child; *p != NONE; p++) {  
    if (*p < N) {
      //printf("what is *p %d, ", *p);
      l2[c2] = *p;            /*child is a splitter node*/
      c2++;
      // printf("%d, ", l2[c2]);
    }
  }

  // loop to build the list of nodes in subtree:
  for (i = 0; i < (k - 1); i++) {
    if ( c2 == 0) return FALSE;     /*There are no child nodes to choose from*/

    w = randm(c2);            /*Random index of node to add*/
    v = l2[w];                /*Node to add*/
    l1[c1] = v; 
    c1++;
    
    for (j = w; j < c2; j++) { /*Delete node v from l2*/  
      l2[j] = l2[j+1];  
    }
    c2--;

    /*Add children of new node v to candidate list l2:*/
    for (p = node[v].child; *p != NONE; p++) {  
      if (*p < N) {
        l2[c2] = *p;
        c2++;
      }
    }
  }

  /*
  printf("\nAfter all loops, (%d) elements in l1: ", k);
    for ( i = 0; i < c1; i++ ) {
      printf("%d, ", *(l1 + i) );
    }
  printf("\n\n");
  */

  /*Now take list of nodes and make a new subtree, still rooted in l1[1]*/
  for (i = (k-1); i > 0; i--) {         /*IMPORTANT: do backwards to not disconnect!*/
    walk_delete(0, *(l1 + i));          /* delete edges in current subtree*/
  }

  // reset and re-use l1/c1 and l2/c2
  l2[0] = l1[0];                    /*l2 is now list of candidate parents*/
  c2 = 1;

  for (i = 0; i < (k-1); i++) {     /*Delete node v from l1*/  
    l1[i] = l1[i+1];                /*l1 is candidate children list*/
  }
  c1 = k-1;

  for (i = 1; i < k; i++) {
    w = randm(c2);                  /*Random index of parent of new edge*/
    u = l2[w];                      /*Parent node*/

    w = randm(c1);                  /*Random index of child of new edge*/
    v = l1[w];                      /*child node*/ 

    insert(node[u].child, v);       /* new edge btw parent and child*/

    /* Update lists and list-sizes: */
    l2[c2] = v;                     /* Add child to parent candidate-list*/
    c2++;

    for (j = w; j < c1; j++) {      /* Delete child from child-cand-list*/  
      l1[j] = l1[j+1];  
    }
    c1--;
  }
  return TRUE;                      /* Succesful */

}

/* ======================================================================
                               showbits
   ====================================================================== */

void showbits(int k)
{
  unsigned int i, j, m;

  i = k;
  m = 0x1;
  for (j = 7; ; j--) {
    if (i & (m << j)) printf("1"); else printf("0");
    if (j == 0) break;
  }
}


/* ======================================================================
                               showqchar
   ====================================================================== */

void showqchar(qchar k)
{
  vchar a;
  int i;

#ifdef TRUE
  a.pi = k;
  for (i = 0; i < BLOCK; i++) {
    showbits(a.c[i]);
    if (i % 16 == 15) printf("\n"); else printf(" ");
  }
  printf("\n");
#endif
}


/* ======================================================================
                               showqint
   ====================================================================== */

void showqint(qchar k)
{
  vchar a;
  int i;

#ifdef TRUE
  a.pi = k;
  for (i = 0; i < BLOCK; i++) {
    printf("%d ", a.c[i]);
  }
  printf("\n");
#endif
}

/* ======================================================================
                               show16int
   ====================================================================== */

void show16int(qchar k)
{
  vchar a;
  int i;

  a.pi = k;
  for (i = 0; i < 16; i+=4) {
    printf("%3d ", a.i[i]);
  }
  printf("\n");
}


/* ======================================================================
                               show8int
   ====================================================================== */

void show8int(__m128i k)
{
  wchar a;
  int i;

  a.pi = k;
  for (i = 0; i < 8; i+=4) {
    printf("%3d ", a.i[i]);
  }
  printf("\n");
}


/* ======================================================================
                               normalize
   ====================================================================== */

int normalize(qchar sums)
{
  vchar v;
  int i, j, sum;

  v.pi = sums;
  sum = 0;
  for (j = 0; j < BLOCK; j++) {
    sum += (int) v.c[j];
  }
  return sum;
}


/* ======================================================================
                               normalize2
   ====================================================================== */

int normalize2(qchar sums)
{
  qchar zero, q, r, s, s1, s2, t, u;
  vchar w;
  int sum;

  zero = _mm256_set1_epi8(0);
  q = sums;
  s1 = _mm256_mpsadbw_epu8(q, zero, 0);
  r = _mm256_srli_si256(q, 8);
  s2 = _mm256_mpsadbw_epu8(r, zero, 0);
  s = _mm256_add_epi16(s1, s2);

  /* now we have 4 sums of 16 bit at char positions 0 8 16 24 */
  t = _mm256_srli_si256(s, 8);
  u = _mm256_add_epi16(t, s);

  /* now we have 2 sums of 16 bit at char positions 0 16 */
  w.pi = u;
  sum = w.i[0] + w.i[8];

#ifdef lort
  printf("sums\n"); showqint(sums);
  printf("s1\n"); show16int(s1);
  printf("s2\n"); show16int(s2);
  printf("t\n"); show16int(t);
  printf("u\n"); show16int(u);
  printf("result %d\n", sum);
#endif

  return sum;
}


/* ======================================================================
                               normalize3
   ====================================================================== */

int normalize3(qchar sums)
{
  qchar zero, q, r, s, s1, s2;
  __m128i t1, t2, t, u, v;
  vchar w;
  int sum;

  zero = _mm256_set1_epi8(0);
  q = sums;
  s1 = _mm256_mpsadbw_epu8(q, zero, 0);
  r = _mm256_srli_si256(q, 8);
  s2 = _mm256_mpsadbw_epu8(r, zero, 0);
  s = _mm256_add_epi16(s1, s2);

  /* now we have 4 sums of 16 bit at char positions 0 8 16 24 */
  t1 = (__m128i) _mm256_extractf128_ps((__m256) s, 1);
  t2 = (__m128i) _mm256_extractf128_ps((__m256) s, 0);
  t = _mm_add_epi16(t1,t2);
  u = _mm_srli_si128(t, 4*2);
  v = _mm_add_epi16(t, u);
  sum = _mm_extract_epi16(v, 0);

#ifdef lort
  printf("r\n"); show16int(r);
  printf("s1\n"); show16int(s1);
  printf("s2\n"); show16int(s2);
  printf("s\n"); show16int(s);
  printf("t1\n"); show8int(t1);
  printf("t2\n"); show8int(t2);
  printf("t\n"); show8int(t);
  printf("u\n"); show8int(u);
  printf("v\n"); show8int(v);
  printf("result %d\n", sum);
#endif

  return sum;
}


/* ======================================================================
                               normalize_first
   ====================================================================== */

qchar normalize_first(qchar sums)
{
  qchar zero, q, r, s, s1, s2;

  zero = _mm256_set1_epi8(0);
  q = sums;
  s1 = _mm256_mpsadbw_epu8(q, zero, 0);
  r = _mm256_srli_si256(q, 8);
  s2 = _mm256_mpsadbw_epu8(r, zero, 0);
  s = _mm256_add_epi16(s1, s2);
  /* now we have 4 sums of 16 bit at char positions 0 8 16 24 */
  return s;
}


/* ======================================================================
                               normalize_second
   ====================================================================== */

int normalize_second(qchar s)
{
  __m128i t1, t2, t, u, v;
  int sum;

  t1 = (__m128i) _mm256_extractf128_ps((__m256) s, 1);
  t2 = (__m128i) _mm256_extractf128_ps((__m256) s, 0);
  t = _mm_add_epi16(t1,t2);
  u = _mm_srli_si128(t, 4*2);
  v = _mm_add_epi16(t, u);
  sum = _mm_extract_epi16(v, 0);
  return sum;
}


/* ======================================================================
                               test_normalize
   ====================================================================== */

void test_normalize()
{
  vchar v;
  qchar a;
  int i, j, sum;

  for (j = 1; j < 10; j++) {
    for (i = 0; i < BLOCK; i++) v.c[i] = randm(10);
    a = v.pi;
    sum = normalize(a);
    printf("sum %d\n", sum);
    sum = normalize2(a);
    printf("sum %d\n", sum);
    sum = normalize3(a);
    printf("sum %d\n", sum);
  }
}


/* ======================================================================
                               nparsimony
   ====================================================================== */

void nparsimony(nodestruct *x, int cardinality)
{
  nodestruct *y;
  unsigned int m, d;
  int i, j, *k, r, s, mx, cost, card, sum[ALPHA];

#ifdef output
  printf("nparsimony %d leaf %d\n", x->id, x->leaf);
#endif

  cost = 0;
  /* for all data */
  for (i = 0; i < S; i++) {
    /* printf("data position %d\n", i); */
    /* find occurrence of each symbol in alphabet */
    for (j = 0; j < ALPHA; j++) {
      m = (unsigned int) 0x1 << (unsigned) j;
      /* printf("%d: mask %d\n", j, m); */
      for (s = 0, k = x->child; *k != NONE; k++) {
        y = node + *k; 
        d = y->data.ch[i];
        if ((d & m) != 0) s++; 
      }
      sum[j] = s;
      /* printf("sum[%d] %d\n", j, s); */
    }

    mx = 0;
    for (j = 0; j < ALPHA; j++) {
      if (sum[j] > mx) mx = sum[j];
    }
    /* printf("mx %d\n", mx); */

    r = 0x0;
    for (s = j = 0; j < ALPHA; j++) {
      if (sum[j] == mx) {
        m = (unsigned int) 0x1 << (unsigned) j;
        r |= m;
      }
    }
    x->data.ch[i] = r;
    cost += (cardinality - mx);
    /* printf("%d ", (cardinality - mx)); */
    /* printf("result %d\n", x->data.ch[i]); */
  }
  /* printf("\n"); */
  /* printf("node cost %d\n", x->sum); */
  x->sum = cost;
}


/* ======================================================================
                               nparsimony256
   ====================================================================== */

void nparsimony256(nodestruct *x, int cardinality)
{
  qchar a, b, c, d, s, t, u, m, res, ss, sum1, sum2, bit1, bit2;
  nodestruct *y;
  int i, *k, sum;

  nparsimonies256++;
#ifdef test
  printf("nparsimony %d leaf %d\n", x->id, x->leaf);
#endif

  /* for all data */
  sum = 0;
  bit1 = _mm256_set1_epi8(0x1);
  bit2 = _mm256_set1_epi8(0x2);
  for (i = 0; i < ST; i++) {
    sum1 = _mm256_set1_epi8(0);
    sum2 = _mm256_set1_epi8(0);
    for (k = x->child; *k != NONE; k++) {
      /* printf("k %d\n", *k); */
      y = node + *k; 
      d = y->data.pck[i];
      a = _mm256_and_si256(bit1, d);
      sum1 = _mm256_add_epi8(sum1, a);
      b = _mm256_and_si256(bit2, d);
      c = _mm256_srli_epi16(b, 1);
      sum2 = _mm256_add_epi8(sum2, c);
    }
    res = _mm256_set1_epi8(0);
    s = _mm256_max_epi8(sum1, sum2);
    sum += normalize3(s);
    m = _mm256_cmpeq_epi8(s, sum1);
    t = _mm256_and_si256(bit1, m);
    res = _mm256_or_si256(res, t);
    m = _mm256_cmpeq_epi8(s, sum2);
    t = _mm256_and_si256(bit2, m);
    res = _mm256_or_si256(res, t);
    x->data.pck[i] = res;
  }
  x->sum = cardinality * S - sum;
  /* printf("node cost %d\n", x->sum); */
}


/* ======================================================================
                               nparsimony256faster
   ====================================================================== */

void nparsimony256faster(nodestruct *x, int cardinality)
{
  qchar a, b, c, d, s, t, u, m, res, ss, sum, sum1, sum2, bit1, bit2;
  nodestruct *y;
  int i, *k, tot;

  nparsimonies256++;
#ifdef test
  printf("nparsimony %d leaf %d\n", x->id, x->leaf);
#endif

  /* for all data */
  sum = _mm256_set1_epi16(0);
  bit1 = _mm256_set1_epi8(0x1);
  bit2 = _mm256_set1_epi8(0x2);
  for (i = 0; i < ST; i++) {
    sum1 = _mm256_set1_epi8(0);
    sum2 = _mm256_set1_epi8(0);
    for (k = x->child; *k != NONE; k++) {
      /* printf("k %d\n", *k); */
      y = node + *k; 
      d = y->data.pck[i];
      a = _mm256_and_si256(bit1, d);
      sum1 = _mm256_add_epi8(sum1, a);
      b = _mm256_and_si256(bit2, d);
      c = _mm256_srli_epi16(b, 1);
      sum2 = _mm256_add_epi8(sum2, c);
    }
    res = _mm256_set1_epi8(0);
    s = _mm256_max_epi8(sum1, sum2);
    ss = normalize_first(s);
    sum = _mm256_add_epi16(sum, ss);
    m = _mm256_cmpeq_epi8(s, sum1);
    t = _mm256_and_si256(bit1, m);
    res = _mm256_or_si256(res, t);
    m = _mm256_cmpeq_epi8(s, sum2);
    t = _mm256_and_si256(bit2, m);
    res = _mm256_or_si256(res, t);
    x->data.pck[i] = res;
  }
  tot = normalize_second(sum); 
  x->sum = cardinality * S - tot;
  /* printf("node cost %d\n", x->sum); */
}

/* ======================================================================
                               nparsimony256faster for clustering
   ====================================================================== */

void nparsimony256faster_c(nodestruct *x, int cardinality)
{
  qchar a, b, c, d, s, t, u, m, res, ss, sum, sum1, sum2, bit1, bit2;
  nodestruct *y;
  int i, *k, tot;

  //nparsimonies256++;
#ifdef test
  printf("nparsimony %d leaf %d\n", x->id, x->leaf);
#endif

  /* for all data */
  sum = _mm256_set1_epi16(0);
  bit1 = _mm256_set1_epi8(0x1);
  bit2 = _mm256_set1_epi8(0x2);
  for (i = 0; i < ST; i++) {
    sum1 = _mm256_set1_epi8(0);
    sum2 = _mm256_set1_epi8(0);
    for (k = x->child; *k != NONE; k++) {
      /* printf("k %d\n", *k); */
      y = nodeC + *k; 
      d = y->data.pck[i];
      a = _mm256_and_si256(bit1, d);
      sum1 = _mm256_add_epi8(sum1, a);
      b = _mm256_and_si256(bit2, d);
      c = _mm256_srli_epi16(b, 1);
      sum2 = _mm256_add_epi8(sum2, c);
    }
    res = _mm256_set1_epi8(0);
    s = _mm256_max_epi8(sum1, sum2);
    ss = normalize_first(s);
    sum = _mm256_add_epi16(sum, ss);
    m = _mm256_cmpeq_epi8(s, sum1);
    t = _mm256_and_si256(bit1, m);
    res = _mm256_or_si256(res, t);
    m = _mm256_cmpeq_epi8(s, sum2);
    t = _mm256_and_si256(bit2, m);
    res = _mm256_or_si256(res, t);
    x->data.pck[i] = res;
  }
  tot = normalize_second(sum); 
  x->sum = cardinality * S - tot;
  /* printf("node cost %d\n", x->sum); */
}


/* ======================================================================
                               parsimony
   ====================================================================== */

void parsimony(bnodestruct *xa, bnodestruct *xb, bnodestruct *x)
{
  qchar a, b, c, d, r, s, t, u, v, one, m, zero, all;
  int i, sum, sux;

#ifdef test
  printf("parsimony %d %d -> %d\n", xa->id, xb->id, x->id);
#endif
  parsimonies++;
  zero = _mm256_set1_epi8(0);
  one = _mm256_set1_epi8(1);

  /* do the work */
  s = zero;
  for (i = 0; i < ST; i++) {
    a = xa->data.pck[i];
    b = xb->data.pck[i];
    c = _mm256_and_si256(a,b);
    d = _mm256_or_si256(a,b);
    m = _mm256_cmpeq_epi8(c,zero);
    r = _mm256_blendv_epi8(c, d, m);
    t = _mm256_blendv_epi8(zero, one, m);
    s = _mm256_add_epi8(s, t);
#ifdef test
    /* u = _mm256_movm_epi8(m); */
    printf("a\n"); showqchar(a);
    printf("b\n"); showqchar(b);
    /* printf("c\n"); showqchar(c); */
    /* printf("d\n"); showqchar(d); */
    /* printf("m\n"); showqchar(m); */
    printf("r\n"); showqchar(r);
    /* printf("t\n"); showqchar(t); */
    printf("s\n"); showqchar(s);
#endif
    x->data.pck[i] = r;
  }
  sum = normalize(s);
#ifdef test
  printf("cost %d\n", sum);
  sux = normalize3(s);
  if (sum != sux) printf("different sums: %d %d\n", sum, sux);
#endif
  x->sum = sum;
}


/* ======================================================================
                               walk_nparsimony
   ====================================================================== */

int walk_nparsimony(int v)
{
  nodestruct *x, *y;
  int *k, sum;

  x = node + v;
  /* printf("walk_nparsimony: node %d vectorized %d\n", x->id, VECT); */

  if (x->leaf) {
    /* printf("walk_nparsimony: is leaf\n"); */
    x->sum = 0;
    x->treesum = 0;
    return 0;
  }

  if (*(x->child) == NONE) {
    /* printf("walk_nparsimony: splitter but no children\n"); */
    x->data = wildcard;
    x->sum = 0;
    x->treesum = 0;
    return 0;
  }
 
 
  // printf("walk_nparsimony node %d: not leaf, %d children\n", v, length(x->child));
  for (k = x->child; *k != NONE; k++) {
    // printf("walk_nparsimony: child %d\n", *k);
    walk_nparsimony(*k);
  }

#ifdef output
  printf("walk_nparsimony: call nparsimon node %d\n", x->id);

  printf("children data\n");
  for (k = x->child; *k != NONE; k++) {
    printf("child %d\n", *k);
    y = node + *k; 
    showdata(y->data);
  }
#endif

  if (VECT == 0) nparsimony(x, length(x->child));
  if (VECT == 1) nparsimony256(x, length(x->child));
  if (VECT == 2) nparsimony256faster(x, length(x->child));

  /* calculate tree cost */
  for (sum = 0, k = x->child; *k != NONE; k++) {
    sum += node[*k].treesum;
  }
  x->treesum = sum + x->sum;
  /* printf("cost treesum %d\n", x->treesum); */

#ifdef output
  printf("result\n");
  showdata(x->data);
#endif

  return x->treesum;
}

/* ======================================================================
                               walk_nparsimony (used for clustering)
   ====================================================================== */

int walk_nparsimony_c(int v)
{
  nodestruct *x, *y;
  int *k, sum;

  x = nodeC + v;
  /* printf("walk_nparsimony: node %d vectorized %d\n", x->id, VECT); */

  if (x->leaf) {
    /* printf("walk_nparsimony: is leaf\n"); */
    x->sum = 0;
    x->treesum = 0;
    return 0;
  }

  if (*(x->child) == NONE) {
    /* printf("walk_nparsimony: splitter but no children\n"); */
    x->data = wildcard;
    x->sum = 0;
    x->treesum = 0;
    return 0;
  }
 
 
  // printf("walk_nparsimony node %d: not leaf, %d children\n", v, length(x->child));
  for (k = x->child; *k != NONE; k++) {
    // printf("walk_nparsimony: child %d\n", *k);
    walk_nparsimony_c(*k);
  }

#ifdef output
  printf("walk_nparsimony: call nparsimon node %d\n", x->id);

  printf("children data\n");
  for (k = x->child; *k != NONE; k++) {
    printf("child %d\n", *k);
    y = nodeC + *k; 
    showdata(y->data);
  }
#endif

  if (VECT == 0) nparsimony(x, length(x->child));
  if (VECT == 1) nparsimony256(x, length(x->child));
  if (VECT == 2) nparsimony256faster_c(x, length(x->child));

  /* calculate tree cost */
  for (sum = 0, k = x->child; *k != NONE; k++) {
    sum += nodeC[*k].treesum;
  }
  x->treesum = sum + x->sum;
  /* printf("cost treesum %d\n", x->treesum); */

#ifdef output
  printf("result\n");
  showdata(x->data);
#endif

  return x->treesum;
}


/* ======================================================================
                               walk_parsimony
   ====================================================================== */

int walk_parsimony(bnodestruct *x)
{
  bnodestruct *xa, *xb;
  int i;

  /* assumption: leaf nodes have two NULL children */
  /*             internal nodes have two real children */

#ifdef TRUE
  printf("node %d left %d right %d\n", x->id, x->left, x->right);
#endif

  if ((x->left == NONE) && (x->right == NONE)) {
    x->sum = 0;
    x->treesum = 0;
    return 0;
  }

#ifdef TRUE
  if (x->left == NONE)  { printf("troubles left\n");  exit(-1); }
  if (x->right == NONE) { printf("troubles right\n"); exit(-1); }
#endif

  xa = bnode + x->left;  walk_parsimony(xa);
  xb = bnode + x->right; walk_parsimony(xb);
  parsimony(xa, xb, x);  /* calculates x->sum */
  x->treesum = xa->treesum + xb->treesum + x->sum;

#ifdef TRUE
  printf("node %d left %d right %d cost %d\n", x->id, x->left, x->right, x->sum);
#endif
  return x->treesum;
}


/* ======================================================================
                               flatwalk_parsimony
   ====================================================================== */

int flatwalk_parsimony(int m)
/* only works if children are evaluated before node */
{
  int k;
  bnodestruct *xa, *xb, *x;

#ifdef output
  printf("\nflatwalk_parsimony %d\n", m);
#endif
  for (k = 0; k <= m; k++) {
    x = bnode + k;
    xa = bnode + x->left;
    xb = bnode + x->right;
#ifdef debug
    printf("flatwalk %d: %d left %d right %d\n", k, x->id, x->left, x->right);
#endif
    if (x->left  >= k) { printf("troubles left index\n"); exit(0); }
    if (x->right >= k) { printf("troubles right index\n"); exit(0); }
    if ((x->left == NONE) && (x->right == NONE)) {
      /* data is already on leaf nodes */
      x->sum = 0;
      x->treesum = 0;
      /* printf("treesum %d\n", x->treesum); */
      continue;
    }
    if (x->left  == NONE) {
#ifdef debug
      printf("left none\n");
#endif
      memcpy(x->data.ch, xb->data.ch, S);
      memset(x->data.ch + S, 0, MAXT-S);
      x->sum = 0;
      x->treesum = xb->treesum;
      /* printf("treesum %d\n", x->treesum); */
      continue;
    } 
    if (x->right == NONE) {
#ifdef debug
      printf("right none\n");
#endif
      memcpy(x->data.ch, xa->data.ch, S);
      memset(x->data.ch + S, 0, MAXT-S);
      x->sum = 0;
      x->treesum = xa->treesum;
      /* printf("treesum %d\n", x->treesum); */
      continue;
    }
#ifdef debug
    printf("binary none\n");
#endif
    parsimony(xa, xb, x);
    x->treesum = xa->treesum + xb->treesum + x->sum;
    /* printf("treesum %d\n", x->treesum); */
  }
  return bnode[m].treesum;
}


/* ======================================================================
				readdata
   ====================================================================== */

int readdata(char *infile)
{
  int i, j, alpha, temp, ok;
  double dist, x, y;
  char sep[100], *p;
  FILE *in;

  /* read all data */
  printf("\nREADDATA '%s'\n", infile);
  in = fopen(infile, "r");
  if (in == NULL) { printf("NO FILE %s\n", infile); exit(-1); }

  ok = fscanf(in, "%d%[,]%d%[,]%d", &N, sep, &M, sep, &V);
  printf("%d %d %d\n", N , M, V);
  if (V > MAXV) { printf("too many nodes\n"); exit(-1); }
  for (i = 0; i < V; i++) node[i].id = i;
  for (i = 0; i < N; i++) node[i].leaf = FALSE;
  for (i = N; i < V; i++) node[i].leaf = TRUE;
  for (i = 0; i < N; i++) node[i].nodetype = SPLITTER;
  for (i = N; i < V; i++) node[i].nodetype = MODEM;

  ok = fscanf(in, "%d%[,]%d", &S, sep, &alpha);
  printf("%d %d\n", S, alpha);
  if (S > MAXT) { printf("too much data\n"); exit(-1); }
  ST = S / BLOCK;
  if (ST * BLOCK != S) { printf("data not multiple of 32\n"); exit(-1); }

  printf("read alphabet %d\n", alpha);
  for (i = 0; i < alpha; i++) {
    ok = fscanf(in, "%d%[, ]", &temp, sep);
  }

  printf("read strings %d %d\n", M, S);
  for (i = 0; i < M; i++) {
    p = node[i+N].data.ch; 
    for (j = 0; j < S; j++, p++) {
      ok = fscanf(in, "%d%[, ]", &temp, sep);
      // printf("%d ", temp);               // Siv hid 23 sep 2022
      *p = (unsigned int) (0x1) << (unsigned int) temp; 
    }
    printf("\n");
  } 
  printf("%d %d %d\n", N , M, V);

  printf("read distances %d\n", V);
  for (i = 0; i < V; i++) {
    for (j = 0; j < V; j++) {
      ok = fscanf(in, "%lf%[, ]", &dist, sep);
    }
  }

  printf("read coordinates %d\n", V);
  for (i = 0; i < V; i++) {
    ok = fscanf(in, "%lf%[,]%lf", &x, sep, &y); 
    printf("%d: (%lf,%lf)\n", i, x, y);
    node[i].x = x;
    node[i].y = y;
  }

  printf("readdata done\n");
  fclose(in);

  /* init wildcard to all states */
  p = wildcard.ch; 
  for (j = 0; j < S; j++, p++) {
    *p = (unsigned int) (0x7);
  } 
  // printf("wildcard\n");              // Siv hid 23 sep 2022
  showdata(wildcard);
}
 

/* ======================================================================
				showparsimonydistance
   ====================================================================== */

void showparsimonydistance()
{
  int i, j;
  bnodestruct a, b, x;

  printf("PARSIMONY DISTANCE\n");
  a.sum = a.treesum = 0;
  b.sum = b.treesum = 0;
  x.sum = x.treesum = 0;
  for (i = N; i < N+M; i++) {
    for (j = N; j < N+M; j++) {
      memcpy(a.data.ch, node[i].data.ch, S);
      memset(a.data.ch + S, 0, MAXT-S);
      memcpy(b.data.ch, node[j].data.ch, S);
      memset(b.data.ch + S, 0, MAXT-S);
      a.id = i; b.id = j; x.id = 0;
      parsimony(&a, &b, &x);
      /* printf("nodes %d %d dist %d\n", i, j, x.sum); */
      printf("%d ", x.sum); 
    }
    printf("\n");
  }
  printf("PARSIMONY DISTANCE DONE\n");
}


/* ======================================================================
				showparsimonytree
   ====================================================================== */

void showparsimonytree()
{
  int k, r;
  bnodestruct *x;

  printf("\nPARSIMONY TREE\n");
  r = general2binary();
  flatwalk_parsimony(r); 
  for (k = 0; k <= r; k++) {
    x = bnode + k;
    if (x->nodetype == AUXILARY) continue;
    
    printf("node %2d: ", x->ref);
    if (x->nodetype == MODEM) {
      printf("MODEM   ");
    }
    if (x->nodetype == SPLITTER) {
      printf("SPLITTER");
    }
    if (x->nodetype == AUXILARY) {
      printf("AUXILARY");
    }
    printf(" pdist %d treedist %d\n", x->sum, x->treesum);
  }
}


/* ======================================================================
				main
   ====================================================================== */


int main(int argc, char *argv[])
/* Main program reads input file names and solves problem */
{
  char filename[100];
  char out_filename[100];
  double s, comptime;
  int i, j, u, v, r, ok, ziter, zsuperiter, zrestart;
  double z, zmin, z1, z2, z1min, z2min, z1base, z2base, alpha, beta;
  double zsuper, z1super, z2super;
  double T, delta, rnd, zold;
  int accept, reject, improved, size;
  

  printf("=======================");
  printf("=======================");
  printf("=======================\n");
  /* read data */
  if (argc == 5) {                    // was 3 before out_file input and beta
    strcpy(filename, argv[1]);
    alpha = atof(argv[2]);
    beta = atof(argv[3]);
    strcpy(out_filename, argv[4]);
    printf("FINDTREE '%s' %f\n", filename, alpha);
    if (VECT == 0) printf("SIMPLE\n");
    if (VECT == 1) printf("VECTORIZED\n"); 
    if (VECT == 2) printf("VECTORIZED FAST\n"); 
  }
  if (argc != 5) {                    // was 3 before out_file input and beta
    printf("input file:"); ok = scanf("%s",filename);
    printf("alpha:"); ok = scanf("%lf",&alpha);
    printf("beta:"); ok = scanf("%lf",&beta);
  }

  srandm(0);
  cputime(NULL);
  /* test_normalize(); */
  zsuper = INFTY; 
  parsimonies = 0;
  nparsimonies = 0;
  nparsimonies256 = 0;

#ifdef debug
  readdata(filename);
  greedy();
  z = walk_cost(0,FALSE);
  printf("cost %f\n", z);
  for (i = 0; i < MAXITER; i++) {
    random_move();
    z = walk_cost(0,FALSE);
    printf("cost %f\n", z);
  }
  walk_show(0);

  printf("general2binary\n");
  general2binary();
  printf("B %d\n", B);
  printf("walk_b_show %d\n", R);
  walk_b_show(R);
  printf("linear_b_show %d\n", R);
  linear_b_show(R);
#endif

  /* ======= real work ====== */
  readdata(filename);
  // showparsimonydistance();       // Siv hid 23 sep 2022

  for (j = 0; j < RESTARTS; j++) {
    printf("------- RESTART %d, z %lf so far best %lf ---------\n", j, z, zsuper);
    srandm(j);
    printf("Set norm base to MST:\n"); 
    greedy();
    printf("walk\n");
    z1base = walk_cost(0,FALSE);
    printf("nparsimony\n");
    z2base = walk_nparsimony(0);
    #ifdef MST
      printf("Contruction: greedy\n");
      greedy(); 
    #endif 
    #ifdef PARSIMONYMST
      printf("Construction: greedy parsi and dist\n");
      greedy_parsi(beta);                      //new construct 
      //return 0;
    #endif
    #ifdef CLUSTER
       printf("Construction: clustering\n");
       greedy_cluster(beta); 
    #endif 
    printf("\nCONSTRUCTION SOLUTION\n");
    walk_show(0);
    printf("walk\n");
    z1 = walk_cost(0,FALSE);
    printf("nparsimony\n");
    z2 = walk_nparsimony(0);

#ifdef old
    printf("convert\n");
    r = general2binary();
    printf("walk_b_show %d\n", r);
    walk_b_show(R);
    z2 = z2base = flatwalk_parsimony(r); 
#endif

    z = alpha * z1 / z1base + (1 - alpha) * z2 / z2base;
    zmin = z; z1min = z1; z2min = z2; ziter = 0;
    printf("z %lf zmin %lf z1 %lf z2 %.0lf\n", z, zmin, z1, z2);  
   
    T = T0; accept = reject = improved = 0; rnd = delta = 0.0;
    zold = z;
    //savetree();                   //Siv added NOOOOO
    //savesolution();               //Siv added nOOOOO
    //zsuper = z;                   //Siv NOOOOOO should alwsy be 1
    for (i = 1; i <= MAXITER; i++) {
      if (i % STEPPRINT == 0) printf("ITERATION %d ", i);
      savetree();

      if (drandm() < 0.7) {
        /*___Neighborhood: Random new edge___*/
        do {
          u = randm(N);                 /* only internal nodes */
          v = randm(V);                 /* internal as well as leaf nodes */
        } while (u == 0 && v >= N);
        insert_edge(u, v);
      } else if (drandm() < 0.85) {
        /*___Neighborhood: Swap parent and child___*/
        do {
          u = randm(N);               /* only internal nodes */
        } while (u == 0);
        // printf("------- Here-------\n");
        swap_parent_child(u);
      } else {
        /*___Neighborhood: New 3/4/5-node-subtree___*/
        do {
          u = randm(N);                 /* only internal nodes */
          s = drandm();
          if (s < 0.333) {              /* v is num nodes in sub-tree*/
            v = 2;
          } else if (s < 0.666) {
            v = 3;
          } else {
            v = 4;
          }
        } while (!shuffle_subtree(u, v));
      }

      #ifdef old
        r = general2binary();
        /* printf("linear_b_show %d\n", r); */
        /* linear_b_show(R); */
        z1 = walk_cost(0,FALSE);
        z2 = flatwalk_parsimony(r); 
        /* z2 = walk_parsimony(bnode+r); */
      #endif

      z1 = walk_cost(0,FALSE);
      z2 = walk_nparsimony(0);

      z = alpha * z1 / z1base + (1 - alpha) * z2 / z2base;
      if (i % STEPPRINT == 0) {
        printf("%s %s z %lf zmin %lf z1 %lf z2 %.0lf ", str1, str2, z, zmin, z1, z2);  
        #ifdef SIMULATEDANNEALING
                printf("temp %lf rnd %lf delta %lf accept %d reject %d improved %d", 
                      T, rnd, delta, accept, reject, improved);
        #endif
        printf("\n");
        accept = reject = improved = 0; 
      }
      if (z < zmin) { 
        zmin = z; z1min = z1; z2min = z2; ziter = i; 
        zold = z;
      }
      if (z < zsuper) {
        zsuper = z; z1super = z1; z2super = z2; zsuperiter = i; zrestart = j;
        savesolution();
      } 

      #ifdef GREEDY
            if (z > zmin) {
              restoretree();
            }
      #endif

      #ifdef SIMULATEDANNEALING
            delta = 1000 * (z - zold);
            if (delta < 0) { 
              improved++;
              zold = z;
            }
            if (delta > 0) { 
              rnd = drandm(); 
              if (rnd < exp(-delta / T)) {
                accept++;
                zold = z;
              } else {
                reject++;
                restoretree();
              }
              /* walk_show(0); */
              /* z2 = walk_nparsimony(0, FALSE);  */
              /* printf("z2 %.0lf\n", z2); */
            }
            T = COOL * T;
      #endif

    }  /* end one restart */
  }  /* end all restarts */
  restoresolution();
  showparsimonytree();

#ifdef debug
  insert_edge(7, 10);
  printf("coordinate 7 (%lf,%lf)\n", node[7].x, node[7].y);
  printf("coordinate 9 (%lf,%lf)\n", node[9].x, node[9].y);
  printf("coordinate 10 (%lf,%lf)\n", node[10].x, node[10].y);
  printf("distance 7,10 %lf\n", dist(7,10));
  printf("distance 9,10 %lf\n", dist(9,10));
  printf("%s %s\n", str1, str2);
  showparsimonytree();
  z1 = walk_cost(0,TRUE);
  z2 = flatwalk_parsimony(r); 
  z = alpha * z1 / z1base + (1 - alpha) * z2 / z2base;
  printf("z %lf z1 %lf z2 %.0lf iteration %d\n", z, z1, z2, ziter);  
#endif
  
  cputime(&comptime);
  printf("\nSOLUTION\n");
  walk_show(0);
  printf("\n'%s' %f\n", filename, alpha);
  printf("\nOBJECTIVE\n");
  printf("zsuper %lf z1super %lf z2super %.0lf found in iteration %d restart %d\n", 
         zsuper, z1super, z2super, zsuperiter, zrestart);  
  printf("zmin %lf z1 %lf z2 %.0lf found in iteration %d\n", 
         zmin, z1min, z2min, ziter);  
  printf("zbase %lf z1base %lf z2base %.0lf\n", 1.0, z1base, z2base);
  printf("cpu time %.2lf seconds\n", comptime);
  printf("parsimonies %ld parsimonies/sec %.0lf\n", 
          nparsimonies256, nparsimonies256 / comptime);
  printf("END\n\n");

  showparsimonydistance();

#ifdef SAVEOUTFILE  
  /* Print sol to file */
  fptr = fopen(out_filename, "a");

  if(fptr == NULL) {
    printf("Error!");   
    exit(1);             
  }
  fseek (fptr, 0, SEEK_END);
  size = ftell(fptr);

  // General info about what this run contains (parameters):
  if (0 == size) {
    fprintf(fptr,"alpha,");
    fprintf(fptr,"beta,");
    fprintf(fptr,"restarts,");
    fprintf(fptr,"maxiter,");
    fprintf(fptr,"construction,");
    fprintf(fptr,"acceptance");
    fprintf(fptr,"\n");
    
    fprintf(fptr,"%f,", alpha);
    fprintf(fptr,"%f,", beta);
    fprintf(fptr,"%d,", RESTARTS);
    fprintf(fptr,"%d,", MAXITER);
    #ifdef CLUSTER
      fprintf(fptr,"cluster,");
    #endif
    #ifdef PARSIMONYMST
      fprintf(fptr,"parsi and dist,");
    #endif
    #ifdef MST
      fprintf(fptr,"dist MST");
    #endif
    #ifdef GREEDY
     fprintf(fptr,"greedy");
    #endif
    #ifdef SIMULATEDANNEALING
     fprintf(fptr,"SA");
    #endif
    fprintf(fptr,"\n");
  }

  // info about this specific run (name and sol)
  fprintf(fptr,"%s,%d,%lf,%.0lf,%lf,%.0lf,%lf,%d,%d", filename, (V-1), z1, z2, z1base, z2base, zsuper, zsuperiter, zrestart);   //name, num edges, dist, parsi, d_base, p_base, obj
  fprintf(fptr,"\n");

  walk_save_file(0);
  fprintf(fptr,"\n");

  fclose(fptr);

  printf("Success print to file\n\n");
#endif

  return 0;
   
}

