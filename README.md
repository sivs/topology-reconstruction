# Topology Reconstruction

This repository contains the code in relation to the paper _Topology reconstruction using time series data in telecommunication networks_. Click [here](https://onlinelibrary.wiley.com/doi/10.1002/net.22196) to access the paper. Make sure to cite the paper if any of the code in this repository is used or modified in any way.
This project only relates to the task of reconstructing (telecommunication) networks based on preprocessed data. If you are working with your own data, make sure to read [this](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=4757707) paper first for inspiration on how to preprocess your data to get more accurate topology reconstruction results.

The code is in a preliminary prototype state and does not contain many explanations nor has redundant functionalities been removed. We strongly suggest that those interested in this project read our paper and make their own implementation from scratch. 

## Getting started


The simulated data for the project is located in the `article_data` folder. Each network consists of two files; an instance file (`HPC_Inst_XXX`) and a solution file (`HPC_Inst_XXX`).
Using the following filename as an example `HFC_Inst_20_3200_2_0.3_seed3_Date_2022_05_24.csv`, then the naming convention is as follows:

 - 20 = number of modems/customers/leaf nodes

 - 3200 = length of the data string for each modem (time steps)

 - 2 = number of unique event types in the modem data. Here, binary data is always considered 

 - 0.3 = is the fraction of (random) data points in each modem data string, where the original bit has been flipped (i.e. added data noise)

 - seed3 = a random seed used when creating the instance. Note the instance generator is not included in this repo.

 - Date_2022_05_24 = yyyy_mm_dd, is the time the instance/sol files were created


The main algorithm is called `findtree.c`. This algorithm performs the neighborhood search for a network instance (`HPC_Inst_XXX`) and returns the best objective value (including the total parsimony score and the total cable length of the best found solution). The algorithm also returns all edges (cable connections) in the best solution. The correctness of the best found solution can then be compared to the true solution stored in the solution file (`HPC_Inst_XXX`).

Below is an example of an (executable) job file running a single instance. This example job file is included as `example_job` in this repo. The job can be executed by running the command: `\. example_job` from your terminal.

```
rm findtreeSiv
gcc -O4 -march=native -o findtreeSiv findtreeSiv.c -lm

./findtreeSiv data_article/HFC_Inst_100_3200_2_0.0_seed7_Date_2022_09_08.csv 0.1 0.5 example_outFile_betaTune_0.5_WithAlpha0.1
```
The first argument is the instance file (including path), second argument is the value of alpha (see objective function from article), the third argument is the value of beta, and the fourth argument is the filename (and path) of the text file the solution will be saved in.

Multiple instance files can readily be run from the same job file by repeating the last line of the above `example_job` with different input arguments. 


### Understanding the output (best found solution)

When running an instance using `findtree.c`, the saved output looks like this:

```
alpha,beta,restarts,maxiter,construction,acceptance
0.100000,0.500000,3,200000,parsi and dist,greedy
data_article/HFC_Inst_100_3200_2_0.0_seed7_Date_2022_09_08.csv,114,9.536921,5919,8.344168,9997,0.612479,49818,0
0,2,0,5,0,14,2,19,2,20,2,74,2,94,2,111,2,18,2,8,8,36,8,102,8,37,8,99,8,38,8,3,8,81,8,87,3,72,3,65,3,75,3,109,3,86,3,23,3,110,3,58,3,60,3,21,3,22,3,89,3,80,5,85,5,28,5,29,5,27,5,96,5,57,5,77,5,93,5,114,5,13,5,11,13,52,13,61,13,53,13,91,13,51,13,90,13,63,11,47,11,45,11,95,11,46,11,92,11,83,14,54,14,98,14,55,14,1,14,4,14,106,14,59,14,56,14,6,1,17,1,67,1,16,1,9,1,12,9,39,9,40,9,41,9,68,12,15,12,48,12,62,12,104,12,50,12,88,12,49,4,66,4,26,4,79,4,73,4,25,4,7,4,10,4,101,4,107,4,108,4,24,7,35,7,34,7,70,7,97,7,33,7,76,7,100,7,78,7,64,10,44,10,113,10,82,10,43,10,103,10,84,10,42,10,112,6,69,6,30,6,105,6,32,6,71,6,31,
```

Line 1 and 2 state some of the run settings. E.g., here alpha was 0.1, beta was 0.5, the number of restarts was 3, the number of iterations was 200,000, the initial solution construction method was _parsi and dist_, and the acceptance method was _greedy_. Settings that are not controlled through the input arguments can be adjusted at the top of `findtree.c` (lines 20-42).<br>
Line 3 lists the instance filename, number of edges in the solution, total cable distance, total parsimony score, distance-based normalization value used in the objective, parsimony-based normalization value used in the objective, objective value, iteration in which best solution was found, and restart number in which best solution was found.<br>
Line 4 lists all of the edges in the solution as a connection between two nodes, one node-pair at a time. Here, the first four edges listed are the edges: (0,2), (0,5), (0,14), and (2, 19).

The 'run settings' from line 1 and 2 will only be written to the output file if it is empty. Thus, if saving multiple instance-runs to the same output file, only line 3 and 4 will be saved for each execution of the algorithm.

### Understanding an instance file

The general content of an instance file  (`HPC_Inst_XXX`):

Line 1:           &nbsp;&nbsp;&nbsp;&nbsp;Number of internal nodes (N), number of leaf nodes (M), total number of nodes (V=N+M)<br>
Line 2:           &nbsp;&nbsp;&nbsp;&nbsp;Length of modem data (T), number of unique characters in data<br>
Line 3:           &nbsp;&nbsp;&nbsp;&nbsp;List of unique characters (alphabet), e.g.: 0,1<br>
Next M lines:     &nbsp;&nbsp;&nbsp;&nbsp;Each containing the T long data string for each modem<br>
Next V lines:     &nbsp;&nbsp;&nbsp;&nbsp;The distance matrix between all V nodes. Node order: [root, internal nodes, leaf nodes] <br>
Last V lines:     &nbsp;&nbsp;&nbsp;&nbsp;X,Y coordinates for all V nodes.  Node order: [root, internal nodes, leaf nodes] <br>


### Understanding a solution file

The general content of a solution file (`HPC_Sol_XXX`):

Line 1:           &nbsp;&nbsp;&nbsp;&nbsp;Number of internal nodes (N), number of leaf nodes (M), total number of nodes (V=N+M)<br>
Next V lines:     &nbsp;&nbsp;&nbsp;&nbsp;Incidence matrix, of true/false. Node order: [root, internal nodes, leaf nodes] <br>
Last V lines:     &nbsp;&nbsp;&nbsp;&nbsp;X,Y coordinates for all V nodes.  Node order: [root, internal nodes, leaf nodes] <br>


The total cable length and parsimony score of the true network topology are not saved in the solution file (`HPC_Sol_XXX`), but can easily be obtained by running `soltreescore.c` like so:

```
rm soltreescore
gcc -O4 -march=native -o soltreescore soltreescore.c -lm

./soltreescore data_article/HFC_Inst_100_3200_2_0.0_seed7_Date_2022_09_08.csv data_article/HFC_Sol_100_3200_2_0.0_seed7_Date_2022_09_08.csv outFile_true_sol
```

The first argument is the instance file (including path), the second argument is the solution file (including path), and the third argument is the filename (and path) of the text file the solution will be saved in. The two last numbers in the first line of the output file are true total cable length and the true parsimony score, respectively.

## Authors and Acknowledgment

The development of this code was supported by Innovation Fund Denmark, project number 0224-00055B, GREENFORCE. The code authors are David Pisinger and Siv Sørensen, both affiliated with the Technical University of Denmark.

